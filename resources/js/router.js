import { createRouter, createWebHistory } from "vue-router";

import Home from "./components/Home";
import About from "./components/About";


const routes = [
    {
        path: '/phonebook/home',
        name: 'home',
        component: Home
    },
    {
        path: '/phonebook/about',
        name: 'about',
        component: About
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})
